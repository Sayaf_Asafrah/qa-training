package FirstProject.FirstProject;

import java.io.IOException;

import org.json.simple.JSONObject;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import junit.framework.Assert;

public class POST_Request {
	
	
	
	
		
		

		
		@Test(dataProvider="DataProvider")
		void POSTCoverPhotos(String id  , String idBook ,String url) {
			
			
		RestAssured.baseURI="https://fakerestapi.azurewebsites.net/api/CoverPhotos";
			
		RequestSpecification httpRequest=RestAssured.given();
		//send POST Request
		JSONObject requestparms=new JSONObject();
		
		requestparms.put("ID", id);
		requestparms.put("IDBook", idBook);
		requestparms.put("Url", url);
		
		
		httpRequest.header("content-type","application/json");
		httpRequest.body(requestparms.toJSONString());
		
		Response response=httpRequest.request(Method.POST,"/create");
		
		String responseBody=response.getBody().asString();
		
		
		System.out.println("Response Body :-"+responseBody);
		
		Assert.assertEquals(responseBody.contains(id),true);
		
		Assert.assertEquals(responseBody.contains(idBook),true);
		Assert.assertEquals(responseBody.contains(url),true);
		
		int statusCode=response.getStatusCode();
		Assert.assertEquals(statusCode,200);
		
		}
		

		
		@DataProvider(name="DataProvider")
		
		String [][] getEmpData() throws IOException{
			
			String path=System.getProperty("user.dir")+"/src/test/java/FirstProject/FirstProject/Test_Data.xls";
			
			int rownum=XLS.getRowCount(path, "sheet1");
		int colcount=	XLS.getCellCount(path, "sheet1", 1);
				
			
			String coverData[][]= new String[rownum][rownum];
			
			for(int i=1;i<=rownum;i++) {
				
				for(int j=0;j<=colcount;j++) {
					
					coverData[i-1][j]=XLS.getCellData(path, "sheet1", i, j);
					
				}
				
				
			}
			
			
			return(coverData);
		
		
		
		
		
		
		
		
	}
	
	
	
	
	
	

}
