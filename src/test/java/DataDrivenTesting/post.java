package DataDrivenTesting;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.json.simple.JSONObject;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.*;
import io.restassured.matcher.RestAssuredMatchers;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import junit.framework.Assert;




public class post {
	
	
	
	@Test
	void POSTCoverPhotos(String id  , String idBook ,String url) {
		
		
	RestAssured.baseURI="https://fakerestapi.azurewebsites.net/api/CoverPhotos";
		
	RequestSpecification httpRequest=RestAssured.given();
	//send POST Request
	JSONObject requestparms=new JSONObject();
	
	requestparms.put("ID", id);
	requestparms.put("IDBook", idBook);
	requestparms.put("Url", url);
	
	
	httpRequest.header("content-type","application/json");
	httpRequest.body(requestparms.toJSONString());
	
	Response response=httpRequest.request(Method.POST,"/create");
	
	String responseBody=response.getBody().asString();
	
	
	System.out.println("Response Body :-"+responseBody);
	
	Assert.assertEquals(responseBody.contains(id),true);
	
	Assert.assertEquals(responseBody.contains(idBook),true);
	Assert.assertEquals(responseBody.contains(url),true);
	
	int statusCode=response.getStatusCode();
	Assert.assertEquals(statusCode,200);
	
	}
	

	
	@DataProvider(name="DataProvider")
	
	String [][] getEmpData(){
		
		String empData[][]= {{"250","250","www.facebook.com"},{"200","200","www.facebook.com"},{"100","100","www.facebook.com"}};
		
		return(empData);
	
	
	
	
	
		
	}
	
	
	
	
	

}
